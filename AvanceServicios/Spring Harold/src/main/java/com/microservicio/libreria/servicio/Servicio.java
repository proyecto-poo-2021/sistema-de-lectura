package com.microservicio.libreria.servicio;

import com.microservicio.libreria.dto.Libro;

import java.util.List;

public interface Servicio {

    public List<Libro> obtenerLibros();
}
