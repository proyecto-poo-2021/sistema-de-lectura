package com.microservicio.libreria.dto;

import lombok.Data;

import java.sql.Date;

@Data
public class Libro {
    private String idLibro;
    private String titulo;
    private String autor;
    private String descripcion;
    private Date fecha;

    public Libro(String idLibro, String titulo) {
        this.idLibro = idLibro;
        this.titulo = titulo;
    }

    public String getIdLibro() {
        return idLibro;
    }

    public void setIdLibro(String idLibro) {
        this.idLibro = idLibro;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
}