package com.microservicio.libreria.controlador;

import com.microservicio.libreria.dto.rest.libro.RespuestaLibro;
import com.microservicio.libreria.servicio.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controlador {

    @Autowired
    private Servicio servicio;

    @RequestMapping(value = "/obtener-Libros",
            method = RequestMethod.POST,
            produces = "application/json;charset=utf-8"
    )

    public @ResponseBody RespuestaLibro obtenerLibros(){
        RespuestaLibro respuestaLibro = new RespuestaLibro();
        respuestaLibro.setLista(servicio.obtenerLibros());
        return respuestaLibro;
    }

}
