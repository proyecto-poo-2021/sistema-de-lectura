package com.microservicio.libreria.dao;

import com.microservicio.libreria.dto.Libro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.*;

@Repository
public class DaoImpl implements Dao{

    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;

    private void obtenerConexion() throws SQLException {
        conexion = jdbcTemplate.getDataSource().getConnection();
    }
    private void cerrarConexion() throws SQLException{
        conexion.close();
        conexion = null;
    }

    @Override
    public List<Libro> obtenerLibros() throws SQLException{
        List<Libro> libros = new ArrayList<>();
        obtenerConexion();
        Statement sentencia = conexion.createStatement();
        String sql =    " SELECT id_libro, titulo "+
                " FROM libro ";
        ResultSet resultado = sentencia.executeQuery(sql);
        while(resultado.next()){
            Libro libro = new Libro(resultado.getString("id_libro"), resultado.getString("titulo"));
            libros.add(libro);
        }
        resultado.close();
        sentencia.close();
        cerrarConexion();
        return libros;
    }

    @Override
    public void insertarLibro(Libro libro) {

    }

    @Override
    public Libro buscarLibro() {
        return null;
    }

    @Override
    public void mostrarLibro(Libro libro) {

    }

}
