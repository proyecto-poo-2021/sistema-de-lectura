package com.microservicio.libreria.servicio;

import com.microservicio.libreria.dao.Dao;
import com.microservicio.libreria.dto.Libro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class ServicioImpl implements Servicio{

    @Autowired
    private Dao dao;

    @Override
    public List<Libro> obtenerLibros() {

        List<Libro> lista = new ArrayList<>();
        try{
            lista = dao.obtenerLibros();
        } catch (SQLException e){
            e.printStackTrace();
        }
        return lista;
    }
}
