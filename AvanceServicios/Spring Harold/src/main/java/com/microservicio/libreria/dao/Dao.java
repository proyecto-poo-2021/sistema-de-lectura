package com.microservicio.libreria.dao;

import com.microservicio.libreria.dto.Libro;

import java.sql.SQLException;
import java.util.List;

public interface Dao {
    public void insertarLibro(Libro libro);
    public Libro buscarLibro();
    public void mostrarLibro(Libro libro);
    public List<Libro> obtenerLibros() throws SQLException;
}
